FROM python:3.10
RUN apt-get update
RUN apt-get install -y figlet
WORKDIR /app
ADD requirements.txt /app
RUN pip install -r requirements.txt
ADD . /app
CMD ["gunicorn", "-b", "0.0.0.0:8080", "webapp:app"]
